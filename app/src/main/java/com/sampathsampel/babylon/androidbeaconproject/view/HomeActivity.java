package com.sampathsampel.babylon.androidbeaconproject.view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.sampathsampel.babylon.androidbeaconproject.R;

public class HomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
    }
}
