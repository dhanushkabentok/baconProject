package com.sampathsampel.babylon.androidbeaconproject.d.component;

import com.sampathsampel.babylon.androidbeaconproject.d.PerActivity;
import com.sampathsampel.babylon.androidbeaconproject.d.module.ActivityModule;
import com.sampathsampel.babylon.androidbeaconproject.view.base.BaseActivity;

import dagger.Component;

/**
 * Created by dhanushka on 10/10/2017.
 */

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(BaseActivity baseActivity);
}
