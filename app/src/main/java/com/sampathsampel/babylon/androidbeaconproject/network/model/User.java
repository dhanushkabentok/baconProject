package com.sampathsampel.babylon.androidbeaconproject.network.model;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by dhanushka on 10/10/2017.
 */

public class User {

    String titel;
    String expirdat;

    public LatLng getmLatLng() {
        return mLatLng;
    }

    public void setmLatLng(LatLng mLatLng) {
        this.mLatLng = mLatLng;
    }

    LatLng mLatLng;

    public String getTitel() {
        return titel;
    }

    public void setTitel(String titel) {
        this.titel = titel;
    }

    public String getExpirdat() {
        return expirdat;
    }

    public void setExpirdat(String expirdat) {
        this.expirdat = expirdat;
    }

    public String getShopname() {
        return shopname;
    }

    public void setShopname(String shopname) {
        this.shopname = shopname;
    }

    public int getCord() {
        return cord;
    }

    public void setCord(int cord) {
        this.cord = cord;
    }

    String shopname;
    int cord;


}
