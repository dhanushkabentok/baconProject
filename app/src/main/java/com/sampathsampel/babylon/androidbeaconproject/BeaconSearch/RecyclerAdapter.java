package com.sampathsampel.babylon.androidbeaconproject.BeaconSearch;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sampathsampel.babylon.androidbeaconproject.DitilseActivity;
import com.sampathsampel.babylon.androidbeaconproject.MainActivity;
import com.sampathsampel.babylon.androidbeaconproject.R;

import java.util.ArrayList;

/*
     Adapter for Recycler View
*/
public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    ArrayList<ArrayList<String>> arr;
    Context mContext;
    private RecyclerAdapter.FriendlistCallback mCallback;

    // Constructor
    public RecyclerAdapter(ArrayList<ArrayList<String>> arr, Context mainActivity, RecyclerAdapter.FriendlistCallback mkkk) {
        this.arr = arr;
        this.mContext = mainActivity;
        this.mCallback = mkkk;
    }

    /*
       View Holder class to instantiate views
     */
    class ViewHolder extends RecyclerView.ViewHolder {

        //UUID
        private TextView name;

        //Major
        private TextView major;

        //Minor
        private TextView licennumber;

        //Distance
        private TextView distance;
        private LinearLayout jfjfj;

        //View Holder Class Constructor
        public ViewHolder(View itemView) {
            super(itemView);

            //Initializing views
            name = itemView.findViewById(R.id.name);
            licennumber = itemView.findViewById(R.id.licennumber);
            distance = itemView.findViewById(R.id.distance);
            jfjfj = itemView.findViewById(R.id.jfjfj);

            View.OnClickListener clickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getAdapterPosition();
                    mCallback.onClicknext( getAdapterPosition());

                }
            };

            itemView.setOnClickListener(clickListener);
        }
    }


    @Override
    public RecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_card_search, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerAdapter.ViewHolder holder, int position) {

        // Getting Array List within respective position
        final ArrayList<String> arrayList = arr.get(position);

        // Checking if arrayList size > 0
        if (arrayList.size() > 0) {

            // Displaying UUID
            holder.name.setText(arrayList.get(0));

            //Displaying Major
            holder.licennumber.setText(arrayList.get(1));

            //Displaying Minor
            holder.distance.setText(arrayList.get(3));

//            holder.jfjfj.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
////
//                }
//            });

        }
    }

    @Override
    public int getItemCount() {
        return arr.size();
    }

    public interface FriendlistCallback {

        void onClicknext( int position);

    }
}