package com.sampathsampel.babylon.androidbeaconproject.network.model;

/**
 * Created by dhanushka on 5/6/18.
 */

public class Fines {
    String amount;
    String date;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    String reason;
}
