package com.sampathsampel.babylon.androidbeaconproject.view;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sampathsampel.babylon.androidbeaconproject.R;
import com.sampathsampel.babylon.androidbeaconproject.network.model.User;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltipWithWrappedWidth;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    List<User> mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mUser = new ArrayList<>();
        setup();
    }

    private void setup() {
        User m = new User();
        m.setCord(1);
        m.setExpirdat("2018/05/23");
        m.setShopname("Shop Name 1");
        m.setTitel("License Expired");
        m.setmLatLng(new LatLng(25.256942, 55.325990));
        mUser.add(m);

        User m2 = new User();
        m2.setCord(2);
        m2.setExpirdat("2018/03/03");
        m2.setShopname("Shop Name 5");
        m2.setTitel("Prievse Expired");
        m2.setmLatLng(new LatLng(25.257858, 55.326389));
        mUser.add(m2);

        User m3 = new User();
        m3.setCord(2);
        m3.setExpirdat("2018/03/13");
        m3.setShopname("Shop Name 4");
        m3.setTitel("Prievse Expired");
        m3.setmLatLng(new LatLng(25.256603, 55.326110));
        mUser.add(m3);
        User m4 = new User();
        m4.setCord(3);
        m4.setExpirdat("2018/07/03");
        m4.setShopname("Shop Name 3");
        m4.setTitel("Prievse Expired");
        m4.setmLatLng(new LatLng(25.256577, 55.325558));
        mUser.add(m4);


    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(25.257577, 55.326801);


        mMap.addMarker(new MarkerOptions().position(sydney).title("User Location").icon(BitmapDescriptorFactory.fromResource(R.drawable.placeholder)));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
//
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

        // Zoom in the Google Map
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(17));
        setUpMap();
    }


    private void setUpMap() {

        for (int i = 0; mUser.size() > i; i++) {

            final View marker = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.activity_friend, null);
            final ImageView uer_imag = (ImageView) marker.findViewById(R.id.imageView);

            if (mUser.get(i).getCord() == 1) {
                uer_imag.setBackgroundResource(R.drawable.ic_action_rd);

            } else if (mUser.get(i).getCord() == 2) {
                uer_imag.setBackgroundResource(R.drawable.ic_action_gren);

            } else if (mUser.get(i).getCord() == 3) {
                uer_imag.setBackgroundResource(R.drawable.ic_action_yelo);

            }

            final LatLng markerLatLng = mUser.get(i).getmLatLng();
            String name = mUser.get(i).getTitel();

            System.out.println("click get one ");


            Marker customMarker = mMap.addMarker(new MarkerOptions()
                    .position(markerLatLng)

                    .icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(this, marker))));


            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker mmarker) {
                    Log.d("mmarker.getTitle()",mmarker.getId());

                    if (mmarker.getId().equalsIgnoreCase("m1")) {
                        displayPopupWindowPin7(0);

                    } else if (mmarker.getId().equalsIgnoreCase("m2")) {
                        displayPopupWindowPin7(2);

                    } else if (mmarker.getId().equalsIgnoreCase("m3")) {
                        displayPopupWindowPin7(2);

                    } else if (mmarker.getId().equalsIgnoreCase("m4")) {
                        displayPopupWindowPin7(3);
                    }
//
                    return false;
                }


            });


            final View mapView = getSupportFragmentManager().findFragmentById(R.id.map).getView();
            if (mapView.getViewTreeObserver().isAlive()) {
                mapView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @SuppressLint("NewApi")
                    // We check which build version we are using.
                    @Override
                    public void onGlobalLayout() {
                        LatLngBounds bounds = new LatLngBounds.Builder().include(markerLatLng).build();
                        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                            // mapView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        } else {
                            /// mapView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        }
                        // mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 0));
//                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(markerLatLng, 20);
//                    map.animateCamera(cameraUpdate);
                    }
                });
            }
        }


    }

    // Convert a view to bitmap
    public static Bitmap createDrawableFromView(Context context, View view) {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        view.setLayoutParams(new Gallery.LayoutParams(Gallery.LayoutParams.WRAP_CONTENT, Gallery.LayoutParams.WRAP_CONTENT));
        view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }


    @SuppressLint("ResourceAsColor")
    private void displayPopupWindowPin7(int v) {
        View layout = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.pin_layout2, null);
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.pin_layout2);
        TextView content = (TextView) dialog.findViewById(R.id.content);
        TextView Previous = (TextView) dialog.findViewById(R.id.Previous);
        RelativeLayout hhh = (RelativeLayout) dialog.findViewById(R.id.hhh);

        if (mUser.get(v).getCord() == 1) {
            hhh.setBackgroundResource(R.drawable.pin_read);
            content.setText(mUser.get(v).getExpirdat());
            Previous.setText(mUser.get(v).getShopname());

        } else if (mUser.get(v).getCord() == 2) {
            hhh.setBackgroundResource(R.drawable.pin_gren);
            content.setText(mUser.get(v).getExpirdat());
            Previous.setText(mUser.get(v).getShopname());

        } else if (mUser.get(v).getCord() == 3) {
            hhh.setBackgroundResource(R.drawable.pin_yelo);
            content.setText(mUser.get(v).getExpirdat());
            Previous.setText(mUser.get(v).getShopname());

        }


        dialog.show();

    }
}
