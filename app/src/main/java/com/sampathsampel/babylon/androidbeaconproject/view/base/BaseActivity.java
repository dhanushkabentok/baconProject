package com.sampathsampel.babylon.androidbeaconproject.view.base;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.sampathsampel.babylon.androidbeaconproject.ProjectApplication;
import com.sampathsampel.babylon.androidbeaconproject.d.component.ActivityComponent;
import com.sampathsampel.babylon.androidbeaconproject.d.component.DaggerActivityComponent;
import com.sampathsampel.babylon.androidbeaconproject.d.module.ActivityModule;
import com.sampathsampel.babylon.androidbeaconproject.network.ApiService;
import com.sampathsampel.babylon.androidbeaconproject.network.CallApi;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

@SuppressLint("Registered")
public abstract class BaseActivity extends AppCompatActivity {

    @Inject
    protected Picasso picasso;

    @Inject
   protected CallApi mDataManager;

    @Inject
    protected ApiService mSicModul;


    private ActivityComponent activityComponent;


    public ActivityComponent getActivityComponent() {
        if (activityComponent == null) {
            activityComponent = DaggerActivityComponent.builder()
                    .activityModule(new ActivityModule(this))
                    .applicationComponent(ProjectApplication.get(this).getcomponent())
                    .build();
        }
        return activityComponent;
    }

//    public ApplicationComponent getActivityComponent() {
//        if (activityComponent == null) {
//             activityComponent = ((ProjectApplication) getApplication()).getcomponent();
//        }
//
//
//        return activityComponent;
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivityComponent().inject(this);
    }
}
