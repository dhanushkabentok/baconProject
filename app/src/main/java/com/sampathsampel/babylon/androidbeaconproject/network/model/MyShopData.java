package com.sampathsampel.babylon.androidbeaconproject.network.model;

import java.util.List;

/**
 * Created by dhanushka on 5/6/18.
 */

public class MyShopData {

    String category;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getExpire_date() {
        return expire_date;
    }

    public void setExpire_date(String expire_date) {
        this.expire_date = expire_date;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getLicense_number() {
        return license_number;
    }

    public void setLicense_number(String license_number) {
        this.license_number = license_number;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getTrade() {
        return trade;
    }

    public void setTrade(String trade) {
        this.trade = trade;
    }

    public List<Permissions> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<Permissions> permissions) {
        this.permissions = permissions;
    }

    public List<Fines> getFines() {
        return fines;
    }

    public void setFines(List<Fines> fines) {
        this.fines = fines;
    }

    String expire_date;
    String lat;
    String lon;
    String license_number;
    String owner;
    String trade;
    List<Permissions>  permissions;
    List<Fines>  fines;
}
