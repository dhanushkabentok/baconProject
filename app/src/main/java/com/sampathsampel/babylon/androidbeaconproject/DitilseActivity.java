package com.sampathsampel.babylon.androidbeaconproject;

import android.app.AlertDialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.RemoteException;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.sampathsampel.babylon.androidbeaconproject.network.model.Fines;
import com.sampathsampel.babylon.androidbeaconproject.network.model.MyShopData;
import com.sampathsampel.babylon.androidbeaconproject.network.model.Permissions;
import com.sampathsampel.babylon.androidbeaconproject.view.MapsActivity;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.Identifier;
import org.altbeacon.beacon.MonitorNotifier;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class DitilseActivity extends AppCompatActivity {
    protected static final String TAG = "MainActvkfkfflfivity";
    private BeaconManager beaconManager;
    private Region BeaconRegion;
    private View mExclusiveEmptyViewHelth, mExclusiveEmptyViewFines;
    LinearLayout mContainerViewHelth, mContainerViewFines;
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
    boolean name9093 = false;
    boolean name9094 = false;
    boolean fmxy09089 = false;
    TextView  Trade, Owner, Category, license_no, expire_date, Permissions, Fines;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        mContainerViewHelth = (LinearLayout) findViewById(R.id.parentView_helth);
        mContainerViewFines = (LinearLayout) findViewById(R.id.parentView_Fines);
        Permissions = (TextView) findViewById(R.id.Permissions);


        Fines = (TextView) findViewById(R.id.Fines);
        Trade = (TextView) findViewById(R.id.Trade);
        Owner = (TextView) findViewById(R.id.Owner);
        Category = (TextView) findViewById(R.id.Category);
        license_no = (TextView) findViewById(R.id.license_no);
        expire_date = (TextView) findViewById(R.id.expire_date);


        Permissions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mContainerViewHelth.getVisibility() == View.VISIBLE) {
                    // Its visible
                    mContainerViewHelth.setVisibility(View.GONE);

                } else {
                    // Either gone or invisible
                    mContainerViewHelth.setVisibility(View.VISIBLE);
                }

            }
        });
        Fines.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mContainerViewFines.getVisibility() == View.VISIBLE) {
                    // Its visible
                    mContainerViewFines.setVisibility(View.GONE);

                } else {
                    // Either gone or invisible
                    mContainerViewFines.setVisibility(View.VISIBLE);
                }

            }
        });
        String s = getIntent().getStringExtra("EXTRA_SESSION_ID");

        setData(loadJSONFromAsset(s));
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d(TAG, "coarse location permission granted");
                } else {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Functionality limited");
                    builder.setMessage("Since location access has not been granted, this app will not be able to discover beacons when in the background.");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {

                        @Override
                        public void onDismiss(DialogInterface dialog) {
                        }

                    });
                    builder.show();
                }
                return;
            }
        }
    }


    public ArrayList<MyShopData> loadJSONFromAsset(String view) {
        ArrayList<MyShopData> locList = new ArrayList<>();
        String json = null;
        try {
            InputStream is = getAssets().open(view);
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        try {
            JSONObject obj = new JSONObject(json);

            MyShopData mMyShopData = new MyShopData();
            mMyShopData.setCategory(obj.getString("category"));
            mMyShopData.setExpire_date(obj.getString("expire_date"));
            mMyShopData.setLat(obj.getString("lat"));
            mMyShopData.setLon(obj.getString("lon"));
            mMyShopData.setLicense_number(obj.getString("license_number"));
            mMyShopData.setOwner(obj.getString("owner"));
            mMyShopData.setTrade(obj.getString("trade"));

            JSONArray m_jArry = obj.getJSONArray("permits");
            JSONArray m_fines = obj.getJSONArray("fines");
            List<Permissions> mListPermissions = new ArrayList();
            List<Fines> mListFines = new ArrayList();
            mListPermissions.clear();
            mListFines.clear();
            for (int i = 0; i < m_jArry.length(); i++) {

                JSONObject jo_inside = m_jArry.getJSONObject(i);
                Permissions mPermissions = new Permissions();
                mPermissions.setDate(jo_inside.getString("date"));
                mPermissions.setType(jo_inside.getString("type"));
                mListPermissions.add(mPermissions);
            }

            for (int i = 0; i < m_fines.length(); i++) {
                JSONObject jo_Fines = m_fines.getJSONObject(i);
                Fines mFines = new Fines();
                mFines.setAmount(jo_Fines.getString("amount"));
                mFines.setDate(jo_Fines.getString("date"));
                mFines.setReason(jo_Fines.getString("reason"));
                mListFines.add(mFines);
            }

            mMyShopData.setPermissions(mListPermissions);
            mMyShopData.setFines(mListFines);
            locList.add(mMyShopData);


        } catch (JSONException e) {
            e.printStackTrace();
        }
        return locList;
    }


    private void inflateEditRowHealth(com.sampathsampel.babylon.androidbeaconproject.network.model.Permissions i) {

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.work_priorityanalysis_add, null);
        TextView valueText = (TextView) rowView.findViewById(R.id.text);
        TextView date_ex = (TextView) rowView.findViewById(R.id.date_ex);
        valueText.setText(i.getType());
        date_ex.setText(i.getDate());

        mExclusiveEmptyViewHelth = rowView;
        mContainerViewHelth.addView(rowView, mContainerViewHelth.getChildCount() - 1);

    }


    private void inflateEditRowFixed(Fines i) {

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View rowView = inflater.inflate(R.layout.work_priorityanalysis_find, null);
        TextView Amount = (TextView) rowView.findViewById(R.id.Amount);
        TextView Reason = (TextView) rowView.findViewById(R.id.Reason);
        TextView Date = (TextView) rowView.findViewById(R.id.date);


        Reason.setText(i.getReason());
        Amount.setText(i.getAmount());
        Date.setText(i.getDate());

        mExclusiveEmptyViewFines = rowView;
        mContainerViewFines.addView(rowView, mContainerViewFines.getChildCount() - 1);

    }


    public void setData(final ArrayList<MyShopData> data) {
        runOnUiThread(new Runnable() {

            @Override
            public void run() {


                Trade.setText(data.get(0).getTrade());
                Owner.setText(data.get(0).getOwner());
                Category.setText(data.get(0).getCategory());
                license_no.setText(data.get(0).getLicense_number());
                expire_date.setText(data.get(0).getExpire_date());

                for (int i = 0; i < data.get(0).getPermissions().size(); i++) {
                    inflateEditRowHealth(data.get(0).getPermissions().get(i));
                }


                for (int y = 0; y < data.get(0).getFines().size(); y++) {
                    inflateEditRowFixed(data.get(0).getFines().get(y));
                }
            }
        });


    }
}