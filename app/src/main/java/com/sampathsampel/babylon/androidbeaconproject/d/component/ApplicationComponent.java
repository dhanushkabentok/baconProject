package com.sampathsampel.babylon.androidbeaconproject.d.component;


import com.sampathsampel.babylon.androidbeaconproject.d.module.ActivityModule;
import com.sampathsampel.babylon.androidbeaconproject.d.module.ApplicarionServicemodule;
import com.sampathsampel.babylon.androidbeaconproject.d.module.PicassoModule;
import com.sampathsampel.babylon.androidbeaconproject.helpers.NetworkAccess;
import com.sampathsampel.babylon.androidbeaconproject.network.ApiService;
import com.sampathsampel.babylon.androidbeaconproject.network.CallApi;
import com.squareup.picasso.Picasso;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by dhanushka on 10/10/2017.
 */

@Singleton
@Component(modules = {ApplicarionServicemodule.class, PicassoModule.class, ActivityModule.class})
public interface ApplicationComponent {

    Picasso getPicasso();
    ApiService getApplicatonService();
    CallApi getClasesink();
    NetworkAccess getNetworkAccess();
}
