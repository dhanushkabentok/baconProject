package com.sampathsampel.babylon.androidbeaconproject.d.module;

import com.sampathsampel.babylon.androidbeaconproject.d.ApplicationScope;
import com.sampathsampel.babylon.androidbeaconproject.network.ApiService;
import com.sampathsampel.babylon.androidbeaconproject.network.DateTimeConverter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.joda.time.DateTime;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by dhanushka on 10/10/2017.
 */
@Module(includes = NetworkModule.class)
public class ApplicarionServicemodule {

    @Provides
    @ApplicationScope
    public ApiService apiService(Retrofit applicatioRetrofit) {

        return applicatioRetrofit.create(ApiService.class);
    }

    @Provides
    @ApplicationScope
    public Gson gson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(DateTime.class, new DateTimeConverter());
        return gsonBuilder.create();
    }

    @Provides
    @ApplicationScope
    public Retrofit retrofit(OkHttpClient okHttpClient, Gson gson) {
        return new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(okHttpClient)
                .baseUrl("https://api.github.com/")
                .build();
    }


}
